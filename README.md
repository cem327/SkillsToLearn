## Overview

This project is a system that retrieves job posts from various online job listing sites, analyzes the words used in these posts, and ranks the skills desired by companies.

## Project Objectives

*   Fetching and storing the content of job posts.
*   Analyzing the words in job posts.
*   Identifying and ranking the skills desired by companies.

## Technologies Used

*   Lombok
*   Hibernate
*   PostgreSQL
*   Node.js
*   Apache Commons CSV
*   Gradle
*   Axios
*   Cheerio

## How to Contribute

You can contact via email: cmylmz327@hotmail.com

